package edu.towson.cosc435.valis.todoapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.gson.Gson
import edu.towson.cosc431.valis.todos.models.Todo
import kotlinx.android.synthetic.main.activity_main.*

class  MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        add_todo_btn.setOnClickListener { launchNewTodoActivity() }
    }

    private fun launchNewTodoActivity() {
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            REQUEST_CODE -> {
                when(resultCode) {
                    Activity.RESULT_OK -> {
                        val todo = Gson().fromJson<Todo>(data?.getStringExtra(NewTodoActivity.TODO), Todo::class.java)
                        Log.d(TAG, todo.toString())
                    }
                }
            }
        }
    }

    companion object {
        val REQUEST_CODE = 1
        val TAG = MainActivity::class.java.simpleName
    }
}
