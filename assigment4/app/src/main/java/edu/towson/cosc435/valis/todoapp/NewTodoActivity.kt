package edu.towson.cosc435.valis.todoapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.gson.Gson
import edu.towson.cosc431.valis.todos.models.Todo
import kotlinx.android.synthetic.main.activity_new_todo.*
import java.util.*

class NewTodoActivity : AppCompatActivity() {

    companion object {
        val TAG = NewTodoActivity::class.java.simpleName
        val TODO = "todo"
    }

    private fun makeTodo() : Todo {
        return Todo(
            content = text_edit_text.editableText.toString(),
            title = title_edit_text.editableText.toString(),
            completed = newtodo_completed_checkbox.isChecked,
            id = UUID.randomUUID().toString()
        )
    }

    private fun saveTodo() {
        val todo = makeTodo()
        val intent = Intent()
        intent.putExtra(TODO, Gson().toJson(todo))
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)

        save_btn.setOnClickListener { saveTodo() }
    }
}
