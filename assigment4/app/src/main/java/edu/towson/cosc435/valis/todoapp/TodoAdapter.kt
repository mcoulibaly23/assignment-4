package edu.towson.cosc435.valis.todoapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class TodoAdapter(private val controller: TodoController, private val repo: TodoRepository) : RecyclerView.Adapter<TodoViewHolder>() {
    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return repo.getCount()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.activity_todo_view, parent, false)
        return TodoViewHolder(view)
    }

}

class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view) {

}