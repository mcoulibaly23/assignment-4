package edu.towson.cosc435.valis.todoapp

import androidx.annotation.IntegerRes

interface TodoController {
    fun deleteTodo(idx: Int)
    fun toggleAwesome(idx: Int)
    fun getSong(idx: Int)
}