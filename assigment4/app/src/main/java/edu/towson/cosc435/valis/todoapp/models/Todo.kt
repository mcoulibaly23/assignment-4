package edu.towson.cosc431.valis.todos.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.time.LocalDateTime
import java.util.*

data class Todo(
        var title: String,
        var content: String,
        var completed: Boolean
        ) {
}